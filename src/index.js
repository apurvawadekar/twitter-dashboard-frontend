import React from 'react';
import ReactDOM from 'react-dom';
import "./assets/css/material-dashboard-react.css"
import App from '../src/container/App.jsx';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
