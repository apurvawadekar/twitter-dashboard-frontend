import React, { Component } from 'react'
import SocialButton from './SocialButton.jsx'


export default class SocialLogin extends Component {

  onLoginSuccess = (user) => {
    console.log(user)
    window.open('/user/dashboard','_self')
  }

  onLoginFailure = (err) => {
    console.error(err)
  }

  onLogoutSuccess = () => {
    console.log('inside onLogoutSuccess')
  }

  onLogoutFailure = (err) => {
    console.error(err)
  }

  logout = () => {
    console.log('inside logout')
    // const { logged, currentProvider } = this.state

    // if (logged && currentProvider) {
    //   this.nodes[currentProvider].props.triggerLogout()
    // }
  }
  
  render () {
    let children

      children = [
        <SocialButton
          provider='facebook'
          appId='1532846240185799'
          onLoginSuccess={this.onLoginSuccess}
          onLoginFailure={this.onLoginFailure}
          onLogoutSuccess={this.onLogoutSuccess}
          key={'facebook'}
        >
          
          <i className={"fab fa-twitter"} />
        </SocialButton>
      ]
    return children
  }

}