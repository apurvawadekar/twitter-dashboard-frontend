import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import user from './user';
import twitter from './twitter';

const reducers = combineReducers({
    routing:routerReducer,
    user: user,
    twitter: twitter,
});

export default reducers